(defgroup capture-screen nil
  "Screenshot command interface."
  :group 'extensions
  :group 'convenience)

(defcustom capture-screen-program "scrot"
  "The program to capture X buffers."
  :type 'string
  :group 'capture-screen)

(defcustom capture-screen-default-dir (concat (getenv "HOME") "/images/")
  "The default dir to save the capture screen files."
  :type 'string
  :group 'capture-screen)

(defcustom capture-screen-default-filename "%Y%m%d%H%M%S_$wx$h_scrot.png"
  "The default file name the capture screen files."
  :type 'string
  :group 'capture-screen)

(defcustom capture-screen-progam-params
  "" ;; "-e 'mv $f ~/Images/'"
  "The arguments to be used with the capture screen program"
  :type 'string
  :group 'capture-screen)

(defcustom capture-screen-select-param
  "--select"
  "The arguments to be used with the capture screen program"
  :type 'string
  :group 'capture-screen)

;; TODO: get a list of arguments instead of a string
(defun capture-screen (&optional args)
  "Captures the screen (X buffer) using printscreen-program."
  (interactive)
  (let ((image-path
         (or args
             (read-file-name "Save to:"
                             capture-screen-default-dir
                             capture-screen-default-filename))))
    (when (shell-command (format "%s %s %s"
                                 capture-screen-program
                                 image-path
                                 capture-screen-progam-params))
      (format "%s" image-path))))

(defun capture-screen-mouse-region (&optional image-path)
  "Captures a region of the screen (X buffer) selecting with the mouse.
This procedure freezes the emacs while the mouse selection is been captured."
  (interactive)
  (let ((image-path
         (or image-path
             (read-file-name "Save to:"
                             capture-screen-default-dir
                             capture-screen-default-filename))))
    (capture-screen (concat (format "'%s' %s"
                                    (expand-file-name image-path)
                                    capture-screen-select-param)))))
